package scripts.Potato.Nodes;

import org.tribot.api2007.Player;

import scripts.Potato.Node;
import scripts.Potato.nvPotato;

public class AntiBan extends Node{

	@Override
	public boolean isValid() {
		return Player.getAnimation() == -1 || Player.isMoving();
		
	}

	@Override
	public void execute() {
		
		nvPotato.abc.performRotateCamera();
		
		nvPotato.abc.performEquipmentCheck();
		
		nvPotato.abc.performExamineObject();
		
		nvPotato.abc.performFriendsCheck();
		
		nvPotato.abc.performPickupMouse();
		
		nvPotato.abc.performRandomRightClick();
		
		nvPotato.abc.performMusicCheck();
		
		nvPotato.abc.performRandomMouseMovement();
		
		nvPotato.abc.performRotateCamera();
		
		nvPotato.abc.performQuestsCheck();
	}

}
