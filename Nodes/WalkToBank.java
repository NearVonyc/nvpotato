package scripts.Potato.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.Potato.Node;
import scripts.Potato.PotAPI;

public class WalkToBank extends Node{

@Override
	public boolean isValid() {
		
		return Inventory.isFull();
	}

	@Override
	public void execute() {
		PotAPI.status = "Walking to Bank";
		if(PotAPI.isInField()){
		WebWalking.walkTo(PotAPI.insideGate1);
		General.sleep(200, 400);
		WebWalking.walkTo(PotAPI.PotGate1);
		General.sleep(100, 200);
		WebWalking.walkToBank();
		}
		
	}

}
