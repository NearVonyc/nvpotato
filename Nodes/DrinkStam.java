package scripts.Potato.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Options;
import org.tribot.api2007.types.RSItem;

import scripts.Potato.Node;
import scripts.Potato.PotAPI;

public class DrinkStam extends Node{

	@Override
	public boolean isValid() {
		
		return PotAPI.shouldDrink() && !PotAPI.isInField();
	}

	@Override
	public void execute() {
		PotAPI.status = "Drinking Stamina";
		if(Game.getRunEnergy() < 10 && PotAPI.StamPots == true){
			PotAPI.status = "Drinking a stam pot";
			RSItem[] stam = Inventory.find("Stamina Potion(4)","Stamina Potion(3)","Stamina Potion(2)","Stamina Potion(1)");
			for(final RSItem stamina : stam){
				if(stamina != null && stamina.isClickable()){
					Timing.waitCondition(new Condition(){

						@Override
						public boolean active() {
							General.sleep(100,400);
							return stamina.click("Drink");
						}
						
					}, General.random(1000, 1500));
					if(!Game.isRunOn()){
					Options.setRunOn(true);
					}
			  }
	    	}
		}

	}

}
