package scripts.Potato;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.util.ABCUtil;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.MessageListening07;
import org.tribot.script.interfaces.Painting;

import scripts.Potato.Node;
import scripts.Potato.Nodes.AntiBan;
import scripts.Potato.Nodes.Bank;
import scripts.Potato.Nodes.DrinkStam;
import scripts.Potato.Nodes.Pick;
import scripts.Potato.Nodes.WalkToBank;
import scripts.Potato.Nodes.WalkToField;
import scripts.PotatoPicker.nvPotatos;
@ScriptManifest(authors = { "John" }, category = "Test",name = "nvPotatoes", description = "Picks potatos near the draynor bank")

public class nvPotato extends Script implements Painting, MessageListening07{

	public static ABCUtil abc = new ABCUtil();
	public static int Mousespeed;
	public ArrayList<Node>nodes = new ArrayList<Node>();


	@Override
	public void onPaint(Graphics g) {
		int perHourC = PotAPI.perHour(PotAPI.countP, this.getRunningTime());
		g.setColor(Color.WHITE);
		g.drawString("NearVonyc's Potato picker", 300, 365);
		g.drawString("Potatos Picked: " + PotAPI.countP + " (/hr: "+ perHourC + " )", 300, 380);
		g.drawString("Time ran: " +Timing.msToString(this.getRunningTime()), 300, 395);
		g.drawString("Status: " + PotAPI.status, 300, 410);
		
	}

	@Override
	public void run() {
		 General.useAntiBanCompliance(true);
		 in();
		 nvPotatos gui = new nvPotatos();
			
			gui.setVisible(true);
			
			
			while(!gui.GUI_Comp){
				sleep(300);
			}
			gui.setVisible(false);
			
			PotAPI.StamPots = gui.StamPots;
			
			Mousespeed = gui.Mousespeed;
		Mouse.setSpeed(Mousespeed);
		while(!PotAPI.stop){
			for(final Node n : nodes){
				if(n.isValid()){
					n.execute();
				}
			}
			sleep(General.random(10, 50));
		}
			
		
	}
	private void in(){
		nodes.add(new Bank());
		nodes.add(new DrinkStam());
		nodes.add(new Pick());
		nodes.add(new WalkToBank());
		nodes.add(new WalkToField());
		nodes.add(new AntiBan());
	}

	@Override
	public void clanMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void duelRequestReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void personalMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMessageReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void serverMessageReceived(String Message) {
		switch(Message)
		{
		
		//Finding out if you die!
		case "You pick a potato.":
			PotAPI.countP++;
			
		}
		
	}
	

	@Override
	public void tradeRequestReceived(String arg0) {
		// TODO Auto-generated method stub
		
	}

}
