package scripts.Potato.Nodes;

import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

import scripts.Potato.Node;
import scripts.Potato.PotAPI;

public class Bank extends Node{

	@Override
	public boolean isValid() {
		
		return Banking.isInBank() && Inventory.isFull();
	}

	@Override
	public void execute() {
		PotAPI.status = "Banking";
		Banking.openBank();
		if(Banking.isBankScreenOpen()){
			Banking.depositAllExcept(PotAPI.stams);
		if(PotAPI.StamPots == true){
		if(Inventory.getCount(PotAPI.stams) == 0){
			Banking.withdraw(1, PotAPI.stams);
		}
		}else{
			Banking.close();
		}
		}
	}

}
