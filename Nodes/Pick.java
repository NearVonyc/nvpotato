package scripts.Potato.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSObject;
import org.tribot.script.interfaces.MessageListening07;

import scripts.Potato.Node;
import scripts.Potato.PotAPI;

public class Pick extends Node{
public ABCUtil abc = new ABCUtil();
	@Override
	public boolean isValid() {
		
		return !Inventory.isFull() && PotAPI.isInField();
	}

	@Override
	public void execute() {
		
		PotAPI.status = "Picking potatos";
		RSObject[]Potato = Objects.findNearest(40, "Potato");
		if(Potato.length>0){
			RSObject potato = Potato[0];
			if(potato.isClickable() && potato.isOnScreen()){
				if(DynamicClicking.clickRSObject(potato, "Pick")){
					PotAPI.status = "Picked Potato";
					General.sleep(200, 300);
					if(WaitforPotato()){
						PotAPI.status = "Succesfull wait";
					}
				}
			}
		}
		
	}
	public boolean WaitforPotato(){
		PotAPI.status = "Waiting for Potato to be picked";
		final int countp = Inventory.getCount("Potato");
		
		return Timing.waitCondition(new Condition(){

			@Override
			public boolean active() {
				
				return Inventory.getCount("Potato") != countp;
			}
			
		}, General.random(2200, 2400));
		
	}
}
	
