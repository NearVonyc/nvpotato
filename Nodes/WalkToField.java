package scripts.Potato.Nodes;

import org.tribot.api.General;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;

import scripts.Potato.Node;
import scripts.Potato.PotAPI;

public class WalkToField extends Node{
public ABCUtil abc = new ABCUtil();
	@Override
	public boolean isValid() {
	
		return !Inventory.isFull() && !PotAPI.isInField();
	}

	@Override
	public void execute() {
		PotAPI.status = "Walking to the potato field";
		WebWalking.walkTo(PotAPI.PotGate1);
		General.sleep(100, 800);
		WebWalking.walkTo(PotAPI.inField2);
	
	}
}

