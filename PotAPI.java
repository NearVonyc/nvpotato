package scripts.Potato;


import org.tribot.api2007.Game;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;

import org.tribot.api2007.types.RSTile;

import scripts.PotatoPicker.nvPotatos;

public class PotAPI {
	public static RSArea Potatos = new RSArea(new RSTile(3139,3290,0), new RSTile(3156,3268,0));
	public static RSArea Potatos2 = new RSArea(new RSTile(3157,3288,0), new RSTile(3138,3270,0));
	public static RSTile PotGate1 = new RSTile(3145,3291,0);
	public static RSTile PotGate2 = new RSTile(3146,3291,0);
	
	public static RSTile[] Field = {new RSTile(3145,3288,0), new RSTile(3145,3284,0)};
	
	public static RSTile insideGate1 = new RSTile(3146,3291,0);
	public static RSTile insideGate2 = new RSTile(3145,3291,0);
	
	public static RSArea lumb = new RSArea(new RSTile(3217, 3224,0), new RSTile(3226, 3213,0));
	
	public static RSTile inField1 = new RSTile(3148,3280,0);
	public static RSTile inField2 = new RSTile(3145,3287,0);
	
	public static int countP;
	
	public nvPotatos gui;

	public static boolean StamPots = false;
	
	public int Mousespeed;
	
	public static boolean GUI_Comp = false;
	public static boolean stop = false;
	public static String status = "";
	
	public static String[] stams = {"Stamina Potion(4)","Stamina Potion(3)","Stamina Potion(2)","Stamina Potion(1)"};
	
	public static int perHour(int gained, long runTime) {
		return (int) (gained * 3600000D / runTime);
	}
	
	public static boolean shouldDrink(){
		if(Game.getRunEnergy()<10 && !Potatos.contains(Player.getPosition()) || !Potatos2.contains(Player.getPosition())){
			return true;
		}
		return false;
	}
	public static boolean isInField(){
		if(Potatos.contains(Player.getPosition()) || Potatos2.contains(Player.getPosition())){
			return true;
		}
		return false;
	}
	
}
