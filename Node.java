package scripts.Potato;

public abstract class Node {
public abstract boolean isValid();

public abstract void execute();
}
